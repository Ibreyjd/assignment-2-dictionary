%include "lib.inc"
%include "words.inc"
%include "dict.inc"
%define buffer_size 255
%define jump_to_key 8


section .bss
buffer: resb buffer_size

section .rodata
input_error: db "Error with input", 0
not_found_error: db "The key wasn't found", 0

section .text
global _start
_start:
    mov rdi, buffer
    mov rsi, buffer_size
    call read_word
    test rdx, rdx
    jz .read_error
    
    mov rdi, buffer
    mov rsi, start_adr   
    call find_word
    test rax, rax
    jz .find_error
    
    add rax, jump_to_key
    push rax
    call string_length
    pop rdx
    add rax, rdx
    add rax, 1
    mov rdi, rax
    call print_string
    call print_newline
    mov rdi, 0
    jmp exit

    .read_error:
    mov rdi, input_error
    call print_error
    call print_newline_stderr
    mov rdi, 1
    jmp exit

    .find_error:
    mov rdi, not_found_error
    call print_error
    call print_newline_stderr
    mov rdi, 1
    jmp exit
