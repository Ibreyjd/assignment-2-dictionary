import subprocess

input_data = ["itmo", "", "name", "sksdak", "aaaaaaaaaabbbbbbbbbbcccccccccceeeeeeeeeeddddddddddaaaaaaaaaabbbbbbbbbbcccccccccceeeeeeeeeeddddddddddaaaaaaaaaabbbbbbbbbbcccccccccceeeeeeeeeeddddddddddaaaaaaaaaabbbbbbbbbbcccccccccceeeeeeeeeeddddddddddaaaaaaaaaabbbbbbbbbbcccccccccceeeeeeeeeedddddddddd123456"]
expected_stdout = ["university ITMO", "", "Maxim", "", ""]
expected_stderr = ["", "Error with input", "", "The key wasn't found", "Error with input"]
flag = True

print("Runming tests ...\n")
for i in range(len(input_data)):
    program = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = program.communicate(input=input_data[i])
    stdout = stdout.strip()
    stderr = stderr.strip()
    if stdout == expected_stdout[i] and stderr == expected_stderr[i]:
        print("Test number " + str(i + 1) + ": passed")
    elif stdout != expected_stdout[i]:
        flag = False
        print("Test number " + str(i + 1) + ": failed")
        print("Expected in stdout: " + expected_stdout[i] + ", but print: " + stdout + ".")
    else:
        flag = False
        print("Test number " + str(i + 1) + ": failed")
        print("Expected in stderr: " + expected_stderr[i] + ", but get: " + stderr)
if flag:
    print("---------------------")
    print("All tests are passed")
