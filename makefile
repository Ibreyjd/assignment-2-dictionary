funkNasm=nasm -g $< -felf64 -o $@
funkLd=ld -o $@ $^

.PHONY: all
all: program

program: lib.o dict.o main.o
	$(funkLd)

%.o: %.asm
	$(funkNasm) 

.PHONY: clean
clean:
	rm -f *.o
	rm -f program

.PHONY: test
test: all
	python test.py
