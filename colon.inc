%define start_adr 0x0

%macro colon 2
    %ifstr %1
        %ifid %2
	    %2:
	        dq start_adr
		db %1, 0

	    %define start_adr %2
	%else
	    %error "2nd word is a label"
	%endif
    %else
	%error "1st word is a string"
    %endif
%endmacro
